# Set Working Directory
# Load Graduation Data
graduation_dat = read.csv("Data/CohortGradRates20172018.csv",header=T)
# Load Fiscal Data
fiscal_dat = read.csv("Data/Budget Transparency Detailed Report FN0 2019.csv",header=T)

# Explore Which Columns we need
names(fiscal_dat)

# We'll use "Funds.Center" to help link our fiscal data to our graduation data which shares the same codes. First we will need to aggregate all the fiscal data for each school.

fiscal_dat_agg = aggregate(as.numeric(fiscal_dat$Total.Cost), by=list(Category=fiscal_dat$Funds.Center),FUN=sum)

# Rename our columns for clarity
names(fiscal_dat_agg) = c("CostCenterCode","Total.Cost")

# Merge our DFs
plot_dat = merge(graduation_dat,fiscal_dat_agg,by="CostCenterCode")

# Create Cost Per Pupil
plot_dat = cbind(plot_dat,plot_dat$Total.Cost/plot_dat$NumberInCohort)
names(plot_dat)[10] = "CostPerPupil"

# Plot our information
plot(plot_dat$CostPerPupil,plot_dat$CohortGradRate, pch=20)

# what are those crazy high values?
head(plot_dat[order(plot_dat$CostPerPupil,decreasing = T),])

# Let's get rid of them for now and discuss them later
bp_costperpupil = boxplot(plot_dat$CostPerPupil)
plot_dat2 = plot_dat[-which(plot_dat$CostPerPupil>bp_costperpupil$stats[5]),]

# Plotting again...
plot(plot_dat2$CostPerPupil,plot_dat2$CohortGradRate, pch=20,
     main = "Cost Per Pupil (Derived) verus Cohort Graduation Rate (Reported)",
     ylab = "Graduation Rate (%)", xlab = "Cost Per Pupil ($USD)"
     )

# Doesn't look good, but what is our Correlation Coefficient
cor(plot_dat2$CostPerPupil,plot_dat2$CohortGradRate)
